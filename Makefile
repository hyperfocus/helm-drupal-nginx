VERSION=0.1.0

# Gcloud
initialize: login set-project

login:
	gcloud auth login

set-project:
	gcloud config set project ${PROJECT}

set-container-cluster:
	gcloud container clusters get-credentials ${GCE_CLUSTER_NAME} --zone ${GCE_ZONE} --project ${PROJECT}

config-list:
	gcloud config list

compute-instances-list:
	gcloud compute instances list

ssh-instance:
	gcloud compute ssh ${INSTANCE}

portainer:
	gcloud compute ssh ${INSTANCE} "docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v data:/data portainer/portainer"

kubectl-proxy:
	kubectl proxy


# Kubectl
check-config:
	kubectl config view

check-context:
	kubectl config current-context


# Default namespace
default: set-context-default use-context-default

set-context-default:
	kubectl config set-context default --namespace=default --cluster=${CLUSTER_VAR} --user=${CLUSTER_VAR}

use-context-default:
	kubectl config use-context default


# Dev namespace
dev: namespace-dev set-context-dev use-context-dev

namespace-dev:
	kubectl create namespace dev

set-context-dev:
	kubectl config set-context dev --namespace=dev --cluster=${CLUSTER_VAR} --user=${CLUSTER_VAR}

use-context-dev:
	kubectl config use-context dev


# Prod namespace
prod: namespace-prod set-context-prod use-context-prod

namespace-prod:
	kubectl create namespace prod

set-context-prod:
	kubectl config set-context prod --namespace=prod --cluster=${CLUSTER_VAR} --user=${CLUSTER_VAR}

use-context-prod:
	kubectl config use-context prod


# Helm
install-helm:
	curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh && \
	chmod 700 get_helm.sh && \
	./get_helm.sh && \
	rm get_helm.sh

init-helm:
	helm init


package: package-drupal repo-index

package-drupal:
	helm package charts/drupal && \
	mv drupal-${VERSION}.tgz docs

repo-index:
	helm repo index docs/


rm: rm-docs

rm-docs:
	rm docs/index.yaml
	rm docs/drupal-${VERSION}.tgz

lint-drupal:
	helm lint charts/drupal

dry-run-drupal:
	helm install --name drupal --dry-run --debug charts/drupal --values charts/drupal/values.yaml --values charts/drupal/prod.yaml

delete-drupal:
	helm delete affiliatelk

drupal-dependencies:
	cd charts/drupal && helm dependency update


# Run on pod
nslookup:
	nslookup ${RELEASE}

# Run on host
get-pods:
	kubectl get pods -l =${APP}

describe-pod:
	kubectl describe pod ${NAME}

logs-drupal-fpm:
	kubectl logs ${NAME} ${DRUPAL_FPM_CONTAINER}

logs-nginx:
	kubectl logs ${NAME} ${NGINX_CONTAINER}

logs-nginx-init:
	kubectl logs ${NAME} copy-nginx-files

logs-drupal-init:
	kubectl logs ${NAME} copy-drupal-files

get-service:
	kubectl get service ${APP} -o json

get-endpoints:
	kubectl get endpoints ${APP}


# Check pvc status
pvc:
	kubectl get pvc

# Check resource capacity
describe-nodes:
	kubectl describe nodes


helm-delete:
	./env-remove.sh && \
	helm delete ${RELEASE}


# https://kubernetes.io/docs/user-guide/kubectl/kubectl_run/
sysdig:
	kubectl run sysdig --image=sysdig/sysdig


# https://cloud.google.com/sql/docs/container-engine-connect
cloud-sql: credentials-file username-password

credentials-file:
	kubectl create secret generic cloudsql-oauth-credentials --from-file=credentials.json=${CLOUD_SQL_CREDENTIALS_FILE}

username-password:
	kubectl create secret generic cloudsql --from-literal=username=${CLOUD_SQL_PROXY_USERNAME} --from-literal=password=${CLOUD_SQL_PROXY_PASSWORD}

# https://kubernetes.io/docs/user-guide/secrets/#creating-a-secret-manually
base64:
	echo "${CLOUD_SQL_CREDENTIALS_FILE}" | base64 -w 0

list-secrets:
	kubectl get secrets

delete-secret:
	kubectl delete secret cloudsql-oauth-credentials

connection-list:
	gcloud sql instances list

connection-name:
	gcloud sql instances describe db


# Gcloud storage
copy-database:
	gsutil cp ${FILE} gs://${BUCKETNAME}


# Helm bug
#
# gcloud config set container/use_application_default_credentials true
# export CLOUDSDK_CONTAINER_USE_APPLICATION_DEFAULT_CREDENTIALS=true


# Custom helm repository
add-custom-repo:
	helm repo add supernami https://supernami.github.io/helm-charts/


# https://github.com/kubernetes/charts/blob/master/stable/mariadb/values.yaml
stable-mariadb:
	helm install --name mariadb3 \
	--set serviceType=ClusterIP,mariadbRootPassword=${DB_ROOT_PASSWORD},mariadbUser=${DB_USER},mariadbPassword=${DB_PASSWORD},mariadbDatabase=${DB_DATABASE} \
	stable/mariadb

# https://github.com/kubernetes/charts/blob/master/stable/traefik/values.yaml
stable-traefik:
	helm install --name traefik --namespace kube-system \
	--set dashboard.enabled=true,dashboard.domain=traefik.${INGRESS_DOMAIN},serviceType=NodePort,cpuRequest=50m stable/traefik


# RBAC
# https://kubernetes.io/docs/admin/authorization/rbac/
#
# allow all user accounts wide permissions
super-user:
	kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  	--clusterrole=cluster-admin \
  	--group=system:serviceaccounts

# get current google identity
get-identity:
	gcloud info | grep Account

# grant cluster-admin to your current identity
grant-permissions:
	kubectl create clusterrolebinding name-cluster-admin \
	--clusterrole=cluster-admin \
	--user=${SERVICE_ACCOUNT_NAME}

# get all cluster role bindings
get-bindings:
	kubectl get clusterrolebindings



# All configmaps
configmaps: main-configmaps default-configmaps

# Configmap kinds
main-configmaps: drupal-nginxconf-configmap
default-configmaps: nginxconfig-default-configmap defaultconf-configmap

# Main configmaps
drupal-nginxconf-configmap:
	kubectl create configmap drupal-nginxconf-1 --from-file=configs/nginx/main/nginx.conf

# Default configmaps
nginxconfig-default-configmap:
	kubectl create configmap default-nginxconf --from-file=configs/nginx/default/nginx.conf

defaultconf-configmap:
	kubectl create configmap defaultconf --from-file=configs/nginx/default/default.conf

# Delete configmaps
delete-configmaps:
	kubectl delete configmap drupal-nginxconf-1



# Gitlab private image pull
# https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
gitlab-secret:
	kubectl create secret docker-registry gitlab-registry \
    	--docker-server=registry.gitlab.com \
    	--docker-username=${GITLAB_EMAIL} \
    	--docker-password=${GITLAB_PERSONAL_ACCESS_TOKEN} \
    	--docker-email=${GITLAB_EMAIL} \
    	--namespace=default

# https://kubernetes.io/docs/admin/authorization/rbac/#permissive-rbac-permissions
permissive-rbac:
	kubectl create clusterrolebinding permissive-binding \
		--clusterrole=cluster-admin \
		--user=admin \
		--user=kubelet \
		--group=system:serviceaccounts